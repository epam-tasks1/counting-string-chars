﻿using System;

namespace CountingStringChars
{
    public static class WhileMethods
    {
        public static int GetSpaceCount(string str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }

            int spaceCount = 0;
            int currentIndex = 0;
            while (currentIndex < str.Length)
            {
                if (char.IsWhiteSpace(str[currentIndex]))
                {
                    spaceCount++;
                }

                currentIndex++;
            }

            return spaceCount;
        }

        public static int GetPunctuationCount(string str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }

            int punctuationCount = 0;
            int currentIndex = 0;
            while (currentIndex < str.Length)
            {
                if (char.IsPunctuation(str[currentIndex]))
                {
                    punctuationCount++;
                }

                currentIndex++;
            }

            return punctuationCount;
        }

        public static int GetSpaceCountRecursive(string str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }

            int result = GetSpaceCountRecursive(str[1..]) + (char.IsWhiteSpace(str[0]) ? 1 : 0);

            return result;
        }

        public static int GetPunctuationCountRecursive(string str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }

            bool isPunctuation = char.IsPunctuation(str[0]);
            int currentIncrement = isPunctuation ? 1 : 0;
            int result = GetPunctuationCountRecursive(str[1..]) + currentIncrement;

            return result;
        }
    }
}
