﻿using System;

namespace CountingStringChars
{
    public static class DoWhileMethods
    {
        public static int GetDigitCount(string str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }

            int currentIndex = 0;
            int digitCount = 0;
            do
            {
                if (char.IsDigit(str[currentIndex]))
                {
                    digitCount++;
                }

                currentIndex++;
            }
            while (currentIndex < str.Length);

            return digitCount;
        }

        public static int GetLetterCount(string str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (string.IsNullOrEmpty(str))
            {
                return 0;
            }

            int currentIndex = 0;
            int letterCount = 0;
            do
            {
                if (char.IsLetter(str[currentIndex]))
                {
                    letterCount++;
                }

                currentIndex++;
            }
            while (currentIndex < str.Length);

            return letterCount;
        }

        public static int GetDigitCountRecursive(string str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            return GetDigitCountRecursive(str, str.Length, 0);
        }

        public static int GetLetterCountRecursive(string str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            return GetLetterCountRecursive(str, str.Length, 0);
        }

        private static int GetDigitCountRecursive(string str, int charsLeft, int counter)
        {
            if (charsLeft > 0)
            {
                return GetDigitCountRecursive(str, charsLeft - 1, char.IsDigit(str[^charsLeft]) ? ++counter : counter);
            }

            return counter;
        }

        private static int GetLetterCountRecursive(string str, int charsLeft, int counter)
        {
            if (charsLeft > 0)
            {
                return GetLetterCountRecursive(str, charsLeft - 1, char.IsLetter(str[^charsLeft]) ? ++counter : counter);
            }

            return counter;
        }
    }
}
